# s3-fsync

## Предварительная подготовка

Необходимо подготовить две (как минимум) ноды.

См  https://docs.k3s.io/quick-start

На одной ноде установить метку `custom/node-reader=true`, на другой `custom/node-writer=true`.

Метка `custom/node-writer=true` должна быть установлена на ноде, где располагаются тестовые данные и скрипты запуска теста.

## Запуск

`./run.sh` - запуск теста.

Для использования `fsync()` необходимо установить переменую окружения `sync=1`.

`./clean.sh` - все почистить.

## Наблюдение

Для наблюдение рекомендуется использовать k9s.

## Стенд

```text
node1 -> Job writer ->  s3-pvc (Тестовые данные в S3)
      |             |-> out-pvc (Копия тестовых данных в S3)
      |-> Minio (s3)
      |-> CSI mounter

node 2 -> Job reader ->  out-pvc
       |-> CSI mounter
```

## Описание тестов

`Job writer` помещает тестовые данные на `out-pvc`, предвариательно очистив его содержимое.
Операция может выполняться без `fsync()` или с ним.

`Job reader` читает тестовые данные из `out-pvc` сразу после завершения `Job writer`.

## Результаты

### Без fsync()

`Job writer`:

```text
real    2m59,805s
user    0m5,587s
sys     0m0,904s
```

Где:

* `rm -rf /out/*`

    ```text
    real    0m0.001s
    user    0m0.001s
    sys    0m0.000s
    ```

* `cp -r /in/unpack /out/`

    ```text
    real    2m49.545s
    user    0m0.600s
    sys    0m9.055s
    ```

`Job reader`:

```text
real    3m35,088s
user    0m6,403s
sys     0m0,910s
```

Где:

* `md5sum --quiet -c /out/sum.txt`

    ```text
    real    3m30.944s
    user    0m4.520s
    sys    0m8.487s
    ```

Во время проведения ипытаний ошибок не было.

### С запуском fsync()

`Job writer`:

```text
real    2m49,190s
user    0m5,149s
sys     0m0,725s
```

Где:

* `rm -rf /out/*`

    ```text
    real    0m0.002s
    user    0m0.001s
    sys    0m0.000s
    ```

* `sync()`

    ```text
    real    0m0.032s
    user    0m0.025s
    sys    0m0.003s
    ```

* `cp -r /in/unpack /out/`

    ```text
    real    2m40.863s
    user    0m0.593s
    sys    0m8.330s
    ```

* `sync()`

    ```text
    real    0m0.064s
    user    0m0.011s
    sys    0m0.008s
    ```

`Job reader`:

```text
real    3m29,932s
user    0m6,065s
sys     0m1,059s
```

Где:

* `md5sum --quiet -c /out/sum.txt`

    ```text
    real    3m24.994s
    user    0m4.611s
    sys    0m8.653s
    ```

Во время проведения ипытаний ошибок не было.
