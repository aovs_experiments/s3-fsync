#!/usr/bin/env bash

set -e

_cur="$(dirname $0)"

_type="local"  # local or s3
[[ -n "$type" ]] && _type="$type"

_sync="0"  # 0 or 1
[[ -n "$sync" ]] && _sync="$sync"


function apply_pvc_by_dir {
    if ! kubectl get pvc "$1" &>/dev/null; then
        kubectl apply -f "$2"
    fi
}

function delete_pvc_by_dir {
    if kubectl get pvc "$1" &>/dev/null; then
        kubectl delete -f "$2"
    fi
}


function apply_jobs_by_dir {
    if ! kubectl get jobs "$1" &>/dev/null; then
        kubectl apply -f "$2"
    fi
}

function delete_jobs_by_dir {
    if kubectl get jobs "$1" &>/dev/null; then
        kubectl delete -f "$2"
    fi
}

function wait {
    local _timeout=100
    local _count=1
    local _action="$1"


    echo "Wait $2 $3"

    while : ; do
        case "$_action" in
            "start")
                if kubectl get "$2" "$3" &>/dev/null; then
                    break
                fi
            ;;
            "stop")
                if ! kubectl get "$2" "$3" &>/dev/null; then
                    break
                fi
            ;;
            "complite")
                if [[ "1" == "$(kubectl get "$2" --template='{{.status.succeeded}}' "$3" 2>/dev/null)" ]]; then
                    break
                fi
            ;;
            "*")
                echo "UNKNOWN ACTION"
                exit 1
            ;;
        esac
        
        if [[ "$_count" -ge "$_timeout" ]]; then
            echo "BREAK BY TIMEOUT"
            break
        fi

        echo -ne .

        (( _count++ ))

        sleep 5
    done

    echo
}


# MINIO

if ! helm status minio &>/dev/null; then
  helm install minio --wait "$_cur/helm/minio/" -f "$_cur/helm/minio-custom-values.yaml"
fi

if ! helm status csi-s3 &>/dev/null; then
    helm install csi-s3 --wait "$_cur/helm/csi-s3/" -f "$_cur/helm/csi-custom-values.yaml"
fi


# Clean

echo "Cleaning..."

delete_jobs_by_dir reader "$_cur/k8s/job-reader"
wait stop deployments reader

delete_jobs_by_dir writer "$_cur/k8s/job-writer"
wait stop deployments writer

delete_pvc_by_dir out-pvc "$_cur/k8s/job-writer"

kubectl delete configmaps sync-map || /bin/true


# COMMON

apply_pvc_by_dir local-in-pvc "$_cur/k8s/volume-in"
wait complite jobs prepare-vol

echo "RUN tests with sync option $_sync"


# WRITER

echo "Run writer on node with label 'custom/node-writer=true'"

kubectl create configmap sync-map --from-literal=sync="$_sync"


apply_jobs_by_dir writer "$_cur/k8s/job-writer"
echo "Run writer..."
time wait complite jobs writer


# READER

echo "Run writer on node with label 'custom/node-reader=true'"

apply_jobs_by_dir reader "$_cur/k8s/job-reader"
echo "Run reader..."
time wait complite jobs reader
