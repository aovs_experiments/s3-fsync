#!/usr/bin/env bash

_cur="$(dirname $0)"

kubectl delete -f "$_cur/k8s/job-reader"
kubectl delete -f "$_cur/./k8s/job-writer"

kubectl delete -f "$_cur/k8s/volume-s3"
kubectl delete -f "$_cur/k8s/volume-local"
kubectl delete -f "$_cur/k8s/volume-in"

kubectl delete configmaps sync-map

helm uninstall csi-s3 minio

kubectl delete pv -n default --all
